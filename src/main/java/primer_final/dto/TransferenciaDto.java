package primer_final.dto;


import primer_final.entity.Transaccion;

import java.util.Date;

public class TransferenciaDto {
    private Long idTransaccion;
    private Date fecha;
    private String nombreCliente;
    private Long cuentaCliente;
    private Long saldoCliente;
    private Long saldoBeneficiario;
    private String nombreBeneficiario;
    private Long cuentaBeneficiaria;
    private Long monto;
    private String rucCliente;


    public TransferenciaDto(Transaccion transaccion) {
        this.idTransaccion = transaccion.getIdTransaccion();
        this.nombreCliente = transaccion.getCuenta().getCliente().getNombre();
        this.cuentaCliente = transaccion.getCuenta().getIdCuenta();
        this.nombreBeneficiario = transaccion.getTransferencia().getCuentaDestino().getCliente().getNombre();
        this.cuentaBeneficiaria = transaccion.getTransferencia().getCuentaDestino().getIdCuenta();
        this.monto = transaccion.getTransferencia().getMonto();
        this.fecha = transaccion.getFecha();
        this.saldoCliente = transaccion.getCuenta().getSaldo();
        this.saldoBeneficiario = transaccion.getTransferencia().getCuentaDestino().getSaldo();
        this.rucCliente = transaccion.getCuenta().getCliente().getRuc();
    }

    public Long getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Long getCuentaCliente() {
        return cuentaCliente;
    }

    public void setCuentaCliente(Long cuentaCliente) {
        this.cuentaCliente = cuentaCliente;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public long getCuentaBeneficiaria() {
        return cuentaBeneficiaria;
    }

    public void setCuentaBeneficiaria(long cuentaBeneficiaria) {
        this.cuentaBeneficiaria = cuentaBeneficiaria;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public Long getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Long saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Long getSaldoBeneficiario() {
        return saldoBeneficiario;
    }

    public void setSaldoBeneficiario(Long saldoBeneficiario) {
        this.saldoBeneficiario = saldoBeneficiario;
    }

    public String getRucCliente() {
        return rucCliente;
    }

    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }    
}
