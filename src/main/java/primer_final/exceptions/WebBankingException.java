package primer_final.exceptions;

public class WebBankingException extends  Exception {
    public WebBankingException(String message) {
        super(message);
    }
}