/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Guille
 */
@Entity
@Table(name = "transferencia")
public class Transferencia implements Serializable {
    @Id
    @SequenceGenerator(name = "ID_TRANSFERENCIA_GENERATOR", sequenceName = "TRANSFERENCIA_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_TRANSFERENCIA_GENERATOR")
    @Column(name = "id_transferencia")
    private Long idTransferencia;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_transaccion", nullable = false)
    private Transaccion transaccion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cuenta_destino", nullable = false)
    private Cuenta cuentaDestino;

    @Column(name = "monto", nullable =  false)
    private Long monto;

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public Cuenta getCuentaDestino() {
        return cuentaDestino;
    }

    public void setCuentaDestino(Cuenta cuentaDestino) {
        this.cuentaDestino = cuentaDestino;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }
}
