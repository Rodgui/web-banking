/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Guille
 */
@Entity
@Table(name = "deposito")
public class Deposito implements Serializable {
    @Id
    @SequenceGenerator(name = "ID_DEPOSITO_GENERATOR", sequenceName = "DEPOSITO_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_DEPOSITO_GENERATOR")
    @Column(name = "id_deposito")
    private Long idDeposito;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_transaccion", nullable = false)
    private Transaccion transaccion;

    @Column(name = "monto", nullable =  false)
    private Long monto;

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }
}
