/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Guille
 */
@Entity
@Table(name = "pago")
public class Pago implements Serializable {
    @Id
    @SequenceGenerator(name = "ID_PAGO_GENERATOR", sequenceName = "PAGO_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_PAGO_GENERATOR")
    @Column(name = "id_pago")
    private Long idPago;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_transaccion", nullable = false)
    private Transaccion transaccion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_servicio", nullable = false)
    private Servicio servicio;

    @Column(name = "monto", nullable =  false)
    private Long monto;

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }
}
