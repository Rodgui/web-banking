package primer_final.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "transaccion")
public class Transaccion implements Serializable {
    @Id
    @SequenceGenerator(name = "ID_TRANSACCION_GENERATOR", sequenceName = "TRANSACCION_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_TRANSACCION_GENERATOR")
    @Column(name = "id_transaccion")
    private Long idTransaccion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cuenta", nullable = false)
    private Cuenta cuenta;

    @Column(name = "fecha", updatable = false, nullable = false)
    private Date fecha;

    @Column(name = "tipo", nullable = false)
    private Character tipo;

    @OneToOne(mappedBy = "transaccion")
    private Deposito deposito;

    @OneToOne(mappedBy = "transaccion")
    private Transferencia transferencia;

    @OneToOne(mappedBy = "transaccion")
    private Pago pago;
    
    public Transaccion() {}

    public Transaccion(Cuenta cuenta, Character tipo) {
        this.cuenta = cuenta;
        this.tipo = tipo;
        this.fecha = new Date();
    }

    public Long getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Long idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Deposito getDeposito() {
        return deposito;
    }

    public void setDeposito(Deposito deposito) {
        this.deposito = deposito;
    }

    public Transferencia getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(Transferencia transferencia) {
        this.transferencia = transferencia;
    }

    public Pago getPago() {
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    @Override
    public String toString() {
        return "Transaccion{" +
                "idTransaccion=" + idTransaccion +
                ", cuenta=" + cuenta +
                ", fecha=" + fecha +
                ", tipo=" + tipo +
                ", deposito=" + deposito +
                ", transferencia=" + transferencia +
                ", pago=" + pago +
                '}';
    }
}
