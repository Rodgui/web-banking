/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Guille
 */
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {
    @Id
    @SequenceGenerator(name = "ID_CLIENTE_GENERATOR", sequenceName = "CLIENTE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_CLIENTE_GENERATOR")
    @Column(name = "id_cliente")
    private Long idCliente;

    @Column(name = "nombre", unique = true, length = 50, nullable = false)
    private String nombre;

    @Column(name = "ruc", unique = true, length = 20, nullable = false)
    private String ruc;

    @Column(name = "direccion", length = 100, nullable = false)
    private String direccion;

    @Column(name = "nro_telefono", length = 8, nullable = false)
    private String nroTelefono;

    @Column(name = "pin_cuenta", nullable = false, length = 74)
    private String pinCuenta;

    @Column(name = "pin_transaccion", nullable = false, length = 74)
    private String pinTransaccion;

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }

    public String getPinCuenta() {
        return pinCuenta;
    }

    public void setPinCuenta(String pinCuenta) {
        this.pinCuenta = pinCuenta;
    }

    public String getPinTransaccion() {
        return pinTransaccion;
    }

    public void setPinTransaccion(String pinTransaccion) {
        this.pinTransaccion = pinTransaccion;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "idCliente=" + idCliente +
                ", nombre='" + nombre + '\'' +
                ", ruc='" + ruc + '\'' +
                ", direccion='" + direccion + '\'' +
                ", nroTelefono='" + nroTelefono + '\'' +
                ", pinCuenta=" + pinCuenta +
                ", pinTransaccion=" + pinTransaccion +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return idCliente.equals(cliente.idCliente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCliente);
    }
}
