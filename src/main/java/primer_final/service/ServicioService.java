package primer_final.service;

import primer_final.entity.Servicio;
import primer_final.repository.ServicioDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioService {
	private final ServicioDao servicioDao;

	public ServicioService(ServicioDao servicioDao) {
		this.servicioDao = servicioDao;
	}

	public List<Servicio> getServicios() {
		return servicioDao.findAll();
	}
}
