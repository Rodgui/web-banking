package primer_final.service;

import primer_final.entity.Cliente;
import primer_final.entity.Cuenta;
import primer_final.exceptions.WebBankingException;
import primer_final.repository.ClienteDao;
import primer_final.repository.CuentaDao;
import primer_final.utils.WebBankingConstants;
import primer_final.utils.WebBankingUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@Service
@Transactional
public class ClienteService {
    private final ClienteDao clienteDao;
    private final CuentaDao cuentaDao;

    public ClienteService(ClienteDao clienteDao, CuentaDao cuentaDao) {
        this.clienteDao = clienteDao;
        this.cuentaDao = cuentaDao;
    }

    public Cliente iniciarSesion(String ruc, String pass) throws WebBankingException {
        Cliente cliente = clienteDao.findByRuc(ruc);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (cliente == null || !bCryptPasswordEncoder.matches(pass, cliente.getPinCuenta()))
            throw new  WebBankingException(WebBankingUtils.getMensaje(WebBankingConstants.MSG_ERROR, "Cuenta y/o pin"));
        return cliente;
    }

    public List<Cuenta> getCuentasByCliente(Long id) {
       return cuentaDao.findByClienteIdCliente(id);
    }

    public Cuenta getCuenta(Long idCuenta) {
        return cuentaDao.findById(idCuenta).orElse(null);
    }
}
