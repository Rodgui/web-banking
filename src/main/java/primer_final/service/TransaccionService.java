package primer_final.service;

import primer_final.entity.Cuenta;
import primer_final.entity.Deposito;
import primer_final.entity.Pago;
import primer_final.entity.Transaccion;
import primer_final.entity.Transferencia;
import primer_final.repository.CuentaDao;
import primer_final.repository.DepositoDao;
import primer_final.repository.PagoDao;
import primer_final.repository.TransaccionDao;
import primer_final.repository.TransferenciaDao;
import primer_final.repository.specs.TransaccionSpecs;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TransaccionService {
	private final TransaccionDao transaccionDao;
	private final DepositoDao depositoDao;
	private final TransferenciaDao transferenciaDao;
	private final PagoDao pagoDao;
	private final CuentaDao cuentaDao;

	public TransaccionService(TransaccionDao transaccionDao, DepositoDao depositoDao, TransferenciaDao transferenciaDao, PagoDao pagoDao, CuentaDao cuentaDao) {
		this.transaccionDao = transaccionDao;
		this.depositoDao = depositoDao;
		this.transferenciaDao = transferenciaDao;
		this.pagoDao = pagoDao;
		this.cuentaDao = cuentaDao;
	}

	public String createDeposito(Transaccion transaccion, Deposito deposito) {
		transaccionDao.save(transaccion);
		depositoDao.save(deposito);
		Cuenta c = transaccion.getCuenta();
		c.setSaldo(c.getSaldo() + deposito.getMonto());
		cuentaDao.save(c);
		return "El deposito ha sido creado exitosamente";
	}

	public String createTransferencia(Transaccion transaccion, Transferencia transferencia) {
		transaccionDao.save(transaccion);
		transferenciaDao.save(transferencia);
		Cuenta cuentaDebitaria = transaccion.getCuenta();
		cuentaDebitaria.setSaldo(cuentaDebitaria.getSaldo() - transferencia.getMonto());

		Cuenta cuentaBeneficiaria = transferencia.getCuentaDestino();
		cuentaBeneficiaria.setSaldo(cuentaBeneficiaria.getSaldo() + transferencia.getMonto());
		cuentaDao.save(cuentaBeneficiaria);
		cuentaDao.save(cuentaDebitaria);

		return "La transferencia ha sido creada exitosamente";
	}

	public String createPago(Transaccion transaccion, Pago pago) {
		transaccionDao.save(transaccion);
		pagoDao.save(pago);
		Cuenta cuenta = transaccion.getCuenta();
		cuenta.setSaldo(cuenta.getSaldo() - pago.getMonto());
		cuentaDao.save(cuenta);
		return "El pago ha sido creado exitosamente";
	}

	public List<Transaccion> getTransacciones(Long idCliente, Date fechaDesde, Date fechaHasta, Character tipo, Cuenta cuenta) {

		Specification<Transaccion> spec = TransaccionSpecs.getByCliente(idCliente);
		spec = spec.and(TransaccionSpecs.getByTipo(tipo));

		if (fechaDesde != null)
			spec = spec.and(TransaccionSpecs.getByFechaDesde(fechaDesde));

		if (fechaHasta != null)
			spec = spec.and(TransaccionSpecs.getByFechaHasta(fechaHasta));
		
		if (cuenta != null)
			spec = spec.and(TransaccionSpecs.getByCuenta(cuenta.getIdCuenta()));

		return transaccionDao.findAll(spec, Sort.by(Sort.Direction.DESC, "fecha"));
	}
}
