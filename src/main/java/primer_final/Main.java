/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final;

import primer_final.gui.MenuGUI;
import primer_final.utils.WebBankingUtils;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.awt.*;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import primer_final.gui.IniciarSesionGUI;

/**
 *
 * @author Guille
 */
@SpringBootApplication
public class Main {
    private static ConfigurableApplicationContext ctx;
	
    public static void main(String[] args) throws IOException {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); 
		} catch (Exception e) {
			System.out.println(e);
		}

        ctx = new SpringApplicationBuilder(Main.class)
                .headless(false).run(args);
        IniciarSesionGUI iniciarSesion = ctx.getBean(IniciarSesionGUI.class);
        MenuGUI menu = ctx.getBean(MenuGUI.class);
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new CardLayout());
        mainPanel.add(iniciarSesion, "iniciarSesion");
        mainPanel.add(menu, "menu");
        
        JFrame frame = new JFrame();
        frame.setIconImage(WebBankingUtils.getImage("logo.png"));
		frame.setTitle("WEB-Banking");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(mainPanel);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
		frame.requestFocus();
    }
}
