package primer_final.repository.specs;

import primer_final.entity.Cliente;
import primer_final.entity.Cuenta;
import primer_final.entity.Transaccion;
import org.springframework.data.jpa.domain.Specification;

import java.util.Calendar;
import java.util.Date;
import javax.persistence.criteria.Join;

public class TransaccionSpecs {
	public static Specification<Transaccion> getByCliente(Long idCliente) {
		return (root, query, criteriaBuilder) -> {
			Join<Transaccion, Cuenta> cuentaJoin = root.join("cuenta");
			Join<Cuenta, Cliente> clienteJoin = cuentaJoin.join("cliente");
			return criteriaBuilder.equal(clienteJoin.get("idCliente"), idCliente);
		};
	}

	public static Specification<Transaccion> getByTipo(Character tipo) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("tipo"), tipo);
	}

	public static Specification<Transaccion> getByFechaDesde(Date fechaDesde) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("fecha"), fechaDesde);
	}

	public static Specification<Transaccion> getByFechaHasta(Date fechaHasta) {
		return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("fecha"), setEndDay(fechaHasta));
	}

	public static Specification<Transaccion> getByCuenta(Long idCuenta) {
		return (root, query, criteriaBuilder) -> {
			Join<Transaccion, Cuenta> cuentaJoin = root.join("cuenta");
			return criteriaBuilder.equal(cuentaJoin.get("idCuenta"), idCuenta);
		};
	}

	private static Date setEndDay(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}
}
