package primer_final.repository;

import primer_final.entity.Pago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PagoDao extends JpaRepository<Pago, Long> {
    List<Pago> getByTransaccionFechaGreaterThanEqualAndTransaccionFechaLessThanEqual(Date fechaDesde, Date fechaHasta);

}
