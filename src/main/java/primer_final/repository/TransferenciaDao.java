package primer_final.repository;

import primer_final.entity.Transferencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TransferenciaDao extends JpaRepository<Transferencia, Long> {
    List<Transferencia> getByTransaccionFechaGreaterThanEqualAndTransaccionFechaLessThanEqual(Date fechaDesde, Date fechaHasta);
}
