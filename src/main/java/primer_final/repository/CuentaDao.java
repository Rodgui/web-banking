package primer_final.repository;

import primer_final.entity.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CuentaDao extends JpaRepository<Cuenta , Long> {
    List<Cuenta> findByClienteIdCliente(Long idCliente);
}
