package primer_final.repository;

import primer_final.entity.Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TransaccionDao extends JpaRepository<Transaccion, Long>, JpaSpecificationExecutor<Transaccion> {
}
