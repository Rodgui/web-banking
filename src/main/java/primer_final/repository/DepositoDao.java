package primer_final.repository;

import primer_final.entity.Deposito;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DepositoDao extends JpaRepository<Deposito, Long> {
    List<Deposito> getByTransaccionFechaGreaterThanEqualAndTransaccionFechaLessThanEqual(Date fechaDesde, Date fechaHasta);
}
