package primer_final.utils;

import primer_final.entity.Transaccion;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import javax.swing.*;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * Clase utilitaria para obtener datos de campos,
 * cargar imagenes y mostrar mensajes
 */
public class WebBankingUtils {
	public static Long getLong(String str) {
		try {
			return Long.parseLong(str);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static String getString(Long n) {
		return NumberFormat.getInstance(Locale.ITALIAN).format(n);
	}

	public static String getMensaje(String msg, String campo) {
		return campo != null ? String.format(msg, campo) : msg;
	}

	public static String getFecha(Date fecha) {
		return new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	}

	public static Function<Transaccion, Object[]> getRows() {
		return t -> {
			List<Object> campos = new ArrayList<>(Arrays.asList(t.getIdTransaccion(),t.getCuenta().getIdCuenta(), WebBankingUtils.getFecha(t.getFecha())));
			Character tipo = t.getTipo();
			if (tipo.equals(WebBankingConstants.TIPO_DEPOSITO))
				campos.add(WebBankingUtils.getString(t.getDeposito().getMonto()));
			if (tipo.equals(WebBankingConstants.TIPO_PAGO)) {
				campos.add(t.getPago().getServicio().getNombre());
				campos.add(WebBankingUtils.getString(t.getPago().getMonto()));
			}
			if (tipo.equals(WebBankingConstants.TIPO_TRANSFERENCIA)) {
				campos.add(t.getTransferencia().getCuentaDestino().getCliente().getNombre());
				campos.add(t.getTransferencia().getCuentaDestino().getIdCuenta());
				campos.add(WebBankingUtils.getString(t.getTransferencia().getMonto()));
			}
			return campos.toArray(new Object[0]);
		};
	}

	public static void showMessage(String msg, String title, int msgType) {
		JOptionPane.showMessageDialog(null, msg, title, msgType);
	}

	public static Image getImage(String imageName) throws IOException {
		return ImageIO.read(WebBankingUtils.class.getResourceAsStream("/img/" + imageName));
	}
}
