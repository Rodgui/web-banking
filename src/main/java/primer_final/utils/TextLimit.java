/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primer_final.utils;

import javax.swing.text.AttributeSet;
import javax.swing.text.PlainDocument;
import javax.swing.text.BadLocationException;

/**
 * Clase para limitar los caracteres ingresados por
 * el usuario a solamente agregar numeros y guiones,
 * ademas de poder limitar cantidad de caracteres
 * aceptados individualmente
 */
public class TextLimit extends PlainDocument {
	private final int limit;

	public TextLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public void insertString(int offs, String str, AttributeSet a)
	throws BadLocationException {
		if (str == null)
			return;
		char aux = str.charAt(0);
		if (aux != '-' && (aux < '0' || aux > '9'))
			return;			
		if ((getLength() + str.length()) <= limit) {
			super.insertString(offs, str, a);
		}
	}
}
