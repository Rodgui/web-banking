package primer_final.utils;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WebBankingConstants {
	//transacciones posibles
	public static final Character TIPO_DEPOSITO = 'D';
	public static final Character TIPO_TRANSFERENCIA = 'T';
	public static final Character TIPO_PAGO = 'P';

	//mensajes posibles
	public static final String MSG_PIN_TRANSACCION = "Ingrese el pin de transaccion";
	public static final String MSG_ERROR = "%s inválido";

	//columnas para tablas posibles
	public static final String[] CUENTA_COLS = {"Nro Cuenta", "Saldo"};
	public static final String[] SERVICIO_COLS = {"Nombre", "Descripcion"};
	public static final String[] DEPOSITO_COLS = {"Transaccion", "Cuenta", "Fecha", "Monto"};
	public static final String[] PAGO_COLS = {"Transaccion", "Cuenta", "Fecha", "Servicio", "Monto"};
	public static final String[] TRANSFERENCIA_COLS = {"Transaccion", "Cuenta", "Fecha", "Cliente Beneficiario", "Cuenta Beneficiario", "Monto"};

	//columnas para historial
	public static final String[][] TRANSACCION_COLS = {DEPOSITO_COLS, TRANSFERENCIA_COLS, PAGO_COLS};

	//mapa para encontrar eficazmente tipo de transaccion
	public static final Map<Character, String> REPORT_MAP = Stream.of(new Object[][]{
		{TIPO_DEPOSITO, "deposito"},
		{TIPO_TRANSFERENCIA, "transferencia"},
		{TIPO_PAGO, "pago"}
	}).collect(Collectors.toMap(data -> (Character) data[0], data -> (String) data[1]));
}
