package primer_final.utils;

import primer_final.exceptions.WebBankingException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.io.InputStream;
import javax.swing.*;

/**
 * Clase de utilidad para mostrar los comprobantes
 * correspondientes a cada operacion
 */
@Service
public class JasperUtils {
	public <T> void mostrarComprobante(List<T> data, Map<String, Object> params, String reportName) throws WebBankingException {
		try {
			InputStream resourceContent = JasperUtils.class.getResourceAsStream("/reports/" + reportName + ".jrxml");

			JasperDesign jasperDesign = JRXmlLoader.load(resourceContent);

			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

			JasperPrint print = JasperFillManager.fillReport(jasperReport, params, new JRBeanCollectionDataSource(data));
			JasperViewer viewer = new JasperViewer(print, false);
			viewer.pack();
			viewer.setLocationRelativeTo(null);
			viewer.setExtendedState(JFrame.MAXIMIZED_BOTH);
			viewer.setVisible(true);
			viewer.setAlwaysOnTop(true);
		} catch (JRException e) {
			throw new WebBankingException("Error al mostrar comprobante");
		}
	}
}
