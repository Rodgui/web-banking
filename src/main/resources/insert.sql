/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Guille
 * Created: 30/10/2020
 */

INSERT INTO CLIENTE VALUES (CLIENTE_SEQ.nextval , 'Avda. Acceso Sur 1440 c. Zavala Cué / Central', 'Carlos Urdapilleta',72116024, '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '5439817-7');
INSERT INTO CLIENTE VALUES (CLIENTE_SEQ.nextval , 'Cerro Corá 782 e. Tacuary y Antequera. / Central', 'Guillermo Rodas',61366993, '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '4630629');
INSERT INTO CLIENTE VALUES (CLIENTE_SEQ.nextval , 'Avda. Cacique Lambare c. Rodriguez de Francia / Central', 'Diego Duarte',81197380, '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '$2a$10$GkPh3PbN18zQ7kwy5nkQ1OUKQflk2L/j1FAz1J6gI5jUKoQPrRfYK', '4549717-5');

INSERT INTO CUENTA VALUES (78914, 8000000, 1);
INSERT INTO CUENTA VALUES (87941 , 15000000, 1);
INSERT INTO CUENTA VALUES (45712 , 50500 , 1);

INSERT INTO CUENTA VALUES (14256 , 90500, 2);
INSERT INTO CUENTA VALUES (52354 , 1500000, 2);

INSERT INTO CUENTA VALUES (15486 , 9800000, 3);
INSERT INTO CUENTA VALUES (98546, 9000000, 3);

INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de luz', 'ANDE');
INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de cuota', 'FPUNA');
INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de deuda', 'INVERFIN');
INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de internet', 'TIGO');
INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de contrato', 'CLARO');
INSERT INTO SERVICIO VALUES (SERVICIO_SEQ.nextval, 'Pago de internet', 'PERSONAL');
